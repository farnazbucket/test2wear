
#include <InternetButton.h>

#include <InternetButton.h>

#include "InternetButton.h"
#include "math.h"


InternetButton b = InternetButton();


int numLights = 11;
bool numLightsChanged = false;


void setup() {

    b.begin();


    Particle.function("lights", controlNumLights);

    
    activateLEDS();

}

void loop(){
    
    
    if(numLightsChanged == true){
        delay(10);
        activateLEDS();
        numLightsChanged = false;
    }
}

void activateLEDS(){

    b.allLedsOff();
    

    for(int i = 11; i <= numLights; i++) {
        
        b.allLedsOn( 245, 102, 66);
        
        delay(10);
        b.allLedsOff();
        for(int i = 11; i<=numLights; i++) {
            b.allLedsOn(86, 121, 209);
            b.allLedsOff();
            delay(1000);
            b.allLedsOff();
            
            
            
            for(int i = 11; i<=numLights; i++){
             b.allLedsOn(126, 242, 231);
             b.allLedsOff();
             delay(2000);
             b.allLedsOff();
             
            }
            
        }
    }
}


int controlNumLights(String command){
    int marking = atoi(command.c_str());

    // Error Checking #1: Check that the number of lights < 11
    // Remember - the device only has 11 lights on it
    
    // Check that the value it's been given is in the right range
    if (marking > 11) {
        marking = 11;
    }
    
    // Error Checking #2: Deal with negative inputs
    if (marking < 10) {
        marking= 10;
    }
    
    
    // If no errors, then set the global variable to numLights
    numLights = marking;

    numLightsChanged = true;

    // In embedded programming, "return 1" means that 
    // the function finished successfully
    return 1;
}